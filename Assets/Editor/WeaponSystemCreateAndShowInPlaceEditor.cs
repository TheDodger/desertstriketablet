﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomEditor(typeof(WeaponSystemCreateAndShowInPlace))]
public class WeaponSystemCreateAndShowInPlaceEditor : Editor
{
    public override void OnInspectorGUI()
    {
        WeaponSystemCreateAndShowInPlace myTarget = (WeaponSystemCreateAndShowInPlace)target;

        DrawDefaultInspector();
		int lCOunt = myTarget.GetComponentsInChildren<Transform> ().Length;
		GUILayout.Button (lCOunt.ToString());
		EditorGUILayout.HelpBox (lCOunt.ToString(), MessageType.Info);
        if (GUILayout.Button("ReLoad Ammo points"))
        {

            myTarget.ReloadAmmoPointsForInspector();
        }
    }
}



